# Changelog

Dossier does its best to use [semantic versioning](http://semver.org).

## v2.1.1

Fix bug: in production, CSV rendering should not contain a backtrace if there's an error.

## v2.1.0

Formatter methods will now be passed a hash of the row values if they accept a second argument. This allows formatting certain rows specially.

## v2.0.1

Switch away from `classify` in determining report name to avoid singularization.

## v2.0.0

First public release (previously internal)
